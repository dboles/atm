#include "DjbAtmHotkeySet.h"

/**
 * SECTION:DjbAtmHotkeySet
 * @Title: DjbAtmHotkeySet
 * @Short_description: a set of labels for the hotkeys of a #DjbAtm
 */
struct _DjbAtmHotkeySet
{
  GObject      parent_instance;

  char        *labels[8];
};

G_DEFINE_TYPE (DjbAtmHotkeySet, djb_atm_hotkey_set, G_TYPE_OBJECT)

enum {
  DJB_ATM_HOTKEY_SET_PROPERTY_LABEL_A = 1,
  DJB_ATM_HOTKEY_SET_PROPERTY_LABEL_B,
  DJB_ATM_HOTKEY_SET_PROPERTY_LABEL_C,
  DJB_ATM_HOTKEY_SET_PROPERTY_LABEL_D,
  DJB_ATM_HOTKEY_SET_PROPERTY_LABEL_E,
  DJB_ATM_HOTKEY_SET_PROPERTY_LABEL_F,
  DJB_ATM_HOTKEY_SET_PROPERTY_LABEL_G,
  DJB_ATM_HOTKEY_SET_PROPERTY_LABEL_H,
  DJB_ATM_HOTKEY_SET_N_PROPERTIES
};

static void djb_atm_hotkey_set_class_init (DjbAtmHotkeySetClass *klass);

static void djb_atm_hotkey_set_init (DjbAtmHotkeySet *set);
static void djb_atm_hotkey_set_finalize (GObject *object);

static void djb_atm_hotkey_set_get_property (GObject *object, unsigned property_id,
                                             GValue *value, GParamSpec *pspec);

static void djb_atm_hotkey_set_set_property (GObject *object, unsigned property_id,
                                             const GValue *value, GParamSpec *pspec);

static GParamSpec *djb_atm_hotkey_set_properties[DJB_ATM_HOTKEY_SET_N_PROPERTIES] = {NULL,};

static void
djb_atm_hotkey_set_class_init (DjbAtmHotkeySetClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = djb_atm_hotkey_set_finalize;

  object_class->get_property = &djb_atm_hotkey_set_get_property;
  object_class->set_property = &djb_atm_hotkey_set_set_property;

  /**
   * DjbAtmHotkeySet:label-a:
   *
   * the label for the 1st hotkey
   */
  djb_atm_hotkey_set_properties[DJB_ATM_HOTKEY_SET_PROPERTY_LABEL_A] =
    g_param_spec_string ("label-a", "Label A",
                         "the label for the 1st hotkey", "",
                         G_PARAM_STATIC_STRINGS |
                           G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE);

  /**
   * DjbAtmHotkeySet:label-b:
   *
   * the label for the 2nd hotkey
   */
  djb_atm_hotkey_set_properties[DJB_ATM_HOTKEY_SET_PROPERTY_LABEL_B] =
    g_param_spec_string ("label-b", "Label B",
                         "the label for the 2nd hotkey", "",
                         G_PARAM_STATIC_STRINGS |
                           G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE);

  /**
   * DjbAtmHotkeySet:label-c:
   *
   * the label for the 3rd hotkey
   */
  djb_atm_hotkey_set_properties[DJB_ATM_HOTKEY_SET_PROPERTY_LABEL_C] =
    g_param_spec_string ("label-c", "Label C",
                         "the label for the 3rd hotkey", "",
                         G_PARAM_STATIC_STRINGS |
                           G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE);

  /**
   * DjbAtmHotkeySet:label-d:
   *
   * the label for the 4th hotkey
   */
  djb_atm_hotkey_set_properties[DJB_ATM_HOTKEY_SET_PROPERTY_LABEL_D] =
    g_param_spec_string ("label-d", "Label d",
                         "the label for the 4th hotkey", "",
                         G_PARAM_STATIC_STRINGS |
                           G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE);

  /**
   * DjbAtmHotkeySet:label-e:
   *
   * the label for the 5th hotkey
   */
  djb_atm_hotkey_set_properties[DJB_ATM_HOTKEY_SET_PROPERTY_LABEL_E] =
    g_param_spec_string ("label-e", "Label E",
                         "the label for the 5th hotkey", "",
                         G_PARAM_STATIC_STRINGS |
                           G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE);

  /**
   * DjbAtmHotkeySet:label-f:
   *
   * the label for the 6th hotkey
   */
  djb_atm_hotkey_set_properties[DJB_ATM_HOTKEY_SET_PROPERTY_LABEL_F] =
    g_param_spec_string ("label-f", "Label F",
                         "the label for the 6th hotkey", "",
                         G_PARAM_STATIC_STRINGS |
                           G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE);

  /**
   * DjbAtmHotkeySet:label-g:
   *
   * the label for the 7th hotkey
   */
  djb_atm_hotkey_set_properties[DJB_ATM_HOTKEY_SET_PROPERTY_LABEL_G] =
    g_param_spec_string ("label-g", "Label G",
                         "the label for the 7th hotkey", "",
                         G_PARAM_STATIC_STRINGS |
                           G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE);

  /**
   * DjbAtmHotkeySet:label-h:
   *
   * the label for the 8th hotkey
   */
  djb_atm_hotkey_set_properties[DJB_ATM_HOTKEY_SET_PROPERTY_LABEL_H] =
    g_param_spec_string ("label-h", "Label H",
                         "the label for the 8th hotkey", "",
                         G_PARAM_STATIC_STRINGS |
                           G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE);

  g_object_class_install_properties (object_class, DJB_ATM_HOTKEY_SET_N_PROPERTIES,
                                     djb_atm_hotkey_set_properties);
}

static void
djb_atm_hotkey_set_init (DjbAtmHotkeySet *atm)
{
}

static void
djb_atm_hotkey_set_finalize (GObject *object)
{
  DjbAtmHotkeySet *set = DJB_ATM_HOTKEY_SET (object);

  for (int i = 0; i < 8; ++i)
    g_free (set->labels[i]);

  G_OBJECT_CLASS (djb_atm_hotkey_set_parent_class)->finalize (object);
}

static void
djb_atm_hotkey_set_get_property (GObject *object, unsigned property_id,
                                 GValue *value, GParamSpec *pspec)
{
  DjbAtmHotkeySet *set = DJB_ATM_HOTKEY_SET (object);

  switch (property_id)
  {
  case DJB_ATM_HOTKEY_SET_PROPERTY_LABEL_A:
  case DJB_ATM_HOTKEY_SET_PROPERTY_LABEL_B:
  case DJB_ATM_HOTKEY_SET_PROPERTY_LABEL_C:
  case DJB_ATM_HOTKEY_SET_PROPERTY_LABEL_D:
  case DJB_ATM_HOTKEY_SET_PROPERTY_LABEL_E:
  case DJB_ATM_HOTKEY_SET_PROPERTY_LABEL_F:
  case DJB_ATM_HOTKEY_SET_PROPERTY_LABEL_G:
  case DJB_ATM_HOTKEY_SET_PROPERTY_LABEL_H:
    {
      const char *label = set->labels[property_id - DJB_ATM_HOTKEY_SET_PROPERTY_LABEL_A];
      g_value_set_string (value, label);
      break;
    }

  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    break;
  }
}

static void
djb_atm_hotkey_set_set_property (GObject *object, unsigned property_id,
                                 const GValue *value, GParamSpec *pspec)
{
  DjbAtmHotkeySet *set = DJB_ATM_HOTKEY_SET (object);

  switch (property_id)
  {
  case DJB_ATM_HOTKEY_SET_PROPERTY_LABEL_A:
  case DJB_ATM_HOTKEY_SET_PROPERTY_LABEL_B:
  case DJB_ATM_HOTKEY_SET_PROPERTY_LABEL_C:
  case DJB_ATM_HOTKEY_SET_PROPERTY_LABEL_D:
  case DJB_ATM_HOTKEY_SET_PROPERTY_LABEL_E:
  case DJB_ATM_HOTKEY_SET_PROPERTY_LABEL_F:
  case DJB_ATM_HOTKEY_SET_PROPERTY_LABEL_G:
  case DJB_ATM_HOTKEY_SET_PROPERTY_LABEL_H:
    {
      char **label = &set->labels[property_id - DJB_ATM_HOTKEY_SET_PROPERTY_LABEL_A];
      *label = g_value_dup_string (value);
      break;
    }

  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    break;
  }
}
