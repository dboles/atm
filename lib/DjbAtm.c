#include "DjbAtm.h"
#include "DjbAtm-enums.h"
#include "DjbAtm-resource.h"

#include "DjbAtmHotkeySet.h"

#include <sqlite3.h>

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define DJB_ATM_ACCOUNT_DIGITS        6 // Keep in sync with .ui files!
#define DJB_ATM_PIN_DIGITS            4 // ditto
#define DJB_ATM_PIN_RETRIES           3
#define DJB_ATM_CURRENCY_SYMBOL     "£"
#define DJB_ATM_WITHDRAW_MULTIPLE  10.0
#define DJB_ATM_WITHDRAW_DAILY    250.0 // Keep these 2 in sync!
#define DJB_ATM_WITHDRAW_DIGITS       3 // ditto above and .ui file

/**
 * SECTION:DjbAtm
 * @Title: DjbAtm
 * @Short_description: a virtual ATM
 */
struct _DjbAtm
{
  GObject      parent_instance;

  char        *database;
  sqlite3     *sqlite;

  DjbAtmState  state; 
  void        *form_context;

  unsigned     n_pin_failures;

  char         account[DJB_ATM_ACCOUNT_DIGITS + 1];
  char         pin    [DJB_ATM_PIN_DIGITS     + 1];
  gboolean     is_manager;

  GtkWindow   *window;
  GtkLabel    *hotkey_labels[8];
  GtkBin      *bin_form;
};

G_DEFINE_TYPE (DjbAtm, djb_atm, G_TYPE_OBJECT)

enum {
  DJB_ATM_PROPERTY_DATABASE = 1,
  DJB_ATM_PROPERTY_STATE,
  DJB_ATM_N_PROPERTIES
};

typedef enum {
  DJB_ATM_ACCOUNT_STATUS_DISABLED,
  DJB_ATM_ACCOUNT_STATUS_ENABLED,
  DJB_ATM_ACCOUNT_STATUS_LOCKED,
} DjbAtmAccountStatus;

typedef struct {
  DjbAtm         *atm;
  DjbAtmButtonId  button_id;
} DjbAtmButtonContext;

typedef struct {
  GtkLabel *label;
  char      digits[DJB_ATM_WITHDRAW_DIGITS + 1];
  double    amount; // TODO: Should this just be an Atm instance variable?
} DjbAtmPromptWithdrawContext;

typedef void     (*DjbAtmFormIniter ) (DjbAtm *, GtkBuilder *);
typedef gboolean (*DjbAtmFormHandler) (DjbAtm *, DjbAtmButtonId);
typedef void     (*DjbAtmFormFreer  ) (DjbAtm *);

typedef struct
{
  DjbAtmFormIniter  initer;
  DjbAtmFormHandler handler;
  DjbAtmFormFreer   freer;
} DjbAtmFormFunctions;

static void djb_atm_class_init (DjbAtmClass *klass);

static void djb_atm_init (DjbAtm *atm);
static void djb_atm_constructed (GObject *object);
static void djb_atm_dispose (GObject *object);
static void djb_atm_finalize (GObject *object);

static void djb_atm_get_property (GObject *object, unsigned property_id,
                                  GValue *value, GParamSpec *pspec);
static void djb_atm_set_property (GObject *object, unsigned property_id,
                                  const GValue *value, GParamSpec *pspec);
static void djb_atm_set_database (DjbAtm *atm, const char *database);
static void djb_atm_set_state    (DjbAtm *atm, DjbAtmState state);

static void djb_atm_form_initer_off             (DjbAtm *atm, GtkBuilder *builder);
static void djb_atm_form_initer_show_welcome    (DjbAtm *atm, GtkBuilder *builder);
static void djb_atm_form_initer_prompt_account  (DjbAtm *atm, GtkBuilder *builder);
static void djb_atm_form_initer_prompt_pin      (DjbAtm *atm, GtkBuilder *builder);
static void djb_atm_form_initer_show_pin_failed (DjbAtm *atm, GtkBuilder *builder);
static void djb_atm_form_initer_prompt_option   (DjbAtm *atm, GtkBuilder *builder);
static void djb_atm_form_initer_show_balance    (DjbAtm *atm, GtkBuilder *builder);
static void djb_atm_form_initer_error_balance   (DjbAtm *atm, GtkBuilder *builder);
static void djb_atm_form_initer_prompt_withdraw (DjbAtm *atm, GtkBuilder *builder);
static void djb_atm_form_initer_show_receipt    (DjbAtm *atm, GtkBuilder *builder);

static gboolean djb_atm_form_handler_off              (DjbAtm *atm, DjbAtmButtonId button_id);
static gboolean djb_atm_form_handler_show_welcome     (DjbAtm *atm, DjbAtmButtonId button_id);
static gboolean djb_atm_form_handler_prompt_account   (DjbAtm *atm, DjbAtmButtonId button_id);
static gboolean djb_atm_form_handler_show_no_account  (DjbAtm *atm, DjbAtmButtonId button_id);
static gboolean djb_atm_form_handler_show_locked      (DjbAtm *atm, DjbAtmButtonId button_id);
static gboolean djb_atm_form_handler_prompt_pin       (DjbAtm *atm, DjbAtmButtonId button_id);
static gboolean djb_atm_form_handler_show_pin_failed  (DjbAtm *atm, DjbAtmButtonId button_id);
static gboolean djb_atm_form_handler_prompt_option    (DjbAtm *atm, DjbAtmButtonId button_id);
static gboolean djb_atm_form_handler_prompt_take_card (DjbAtm *atm, DjbAtmButtonId button_id);
static gboolean djb_atm_form_handler_show_balance     (DjbAtm *atm, DjbAtmButtonId button_id);
static gboolean djb_atm_form_handler_prompt_withdraw  (DjbAtm *atm, DjbAtmButtonId button_id);
static gboolean djb_atm_form_handler_prompt_wdraw_rcp (DjbAtm *atm, DjbAtmButtonId button_id);
static gboolean djb_atm_form_handler_show_receipt     (DjbAtm *atm, DjbAtmButtonId button_id);
static gboolean djb_atm_form_handler_prompt_take_cash (DjbAtm *atm, DjbAtmButtonId button_id);
static gboolean djb_atm_form_handler_prompt_shutdown  (DjbAtm *atm, DjbAtmButtonId button_id);

static void djb_atm_form_freer_prompt_withdraw (DjbAtm *atm);

static void     djb_atm_form_handler_prompt_account_clear  (DjbAtm *atm);
static gboolean djb_atm_form_handler_prompt_account_append (DjbAtm *atm, unsigned number);
static gboolean djb_atm_form_handler_prompt_account_enter  (DjbAtm *atm);

static void     djb_atm_form_handler_prompt_pin_clear  (DjbAtm *atm);
static gboolean djb_atm_form_handler_prompt_pin_append (DjbAtm *atm, unsigned number);
static gboolean djb_atm_form_handler_prompt_pin_enter  (DjbAtm *atm);

static void     djb_atm_form_initer_show_balance_impl (DjbAtm *atm, GtkBuilder *builder,
                                                       const char *label_main);

static void     djb_atm_form_handler_prompt_withdraw_clear  (DjbAtm *atm);
static gboolean djb_atm_form_handler_prompt_withdraw_append (DjbAtm *atm, unsigned number);
static gboolean djb_atm_form_handler_prompt_withdraw_enter  (DjbAtm *atm,
                                                             gboolean with_receipt);
static gboolean djb_atm_form_handler_prompt_withdraw_impl   (DjbAtm *atm,
                                                             DjbAtmButtonId button_id,
                                                             gboolean with_receipt);
static void     djb_atm_form_handler_prompt_withdraw_do     (DjbAtm *atm, double amount,
                                                             gboolean with_receipt);

static void     djb_atm_beep              (DjbAtm *atm);
static void     djb_atm_clear_login       (DjbAtm *atm);
static void     djb_atm_get_balances      (DjbAtm *atm, double *out_balance,
                                           double *out_withdrawn, double *out_available);
static void     djb_atm_on_button_clicked (DjbAtmButtonContext *context);
static gboolean djb_atm_on_window_delete  (DjbAtm *atm);
static void     djb_atm_set_hotkey_label  (DjbAtm *atm, DjbAtmButtonId button_id,
                                           const char *text);
static int      djb_atm_sqlite3_step      (DjbAtm *atm, sqlite3_stmt *stmt, gboolean allow_row);

static GParamSpec *djb_atm_properties[DJB_ATM_N_PROPERTIES] = {NULL,};

static const DjbAtmFormFunctions djb_atm_form_functions[DJB_ATM_N_STATES] = {
  {djb_atm_form_initer_off             , djb_atm_form_handler_off             , NULL},
  {djb_atm_form_initer_show_welcome    , djb_atm_form_handler_show_welcome    , NULL},
  {djb_atm_form_initer_prompt_account  , djb_atm_form_handler_prompt_account  , NULL},
  {NULL                                , djb_atm_form_handler_show_no_account , NULL},
  {NULL                                , djb_atm_form_handler_show_locked     , NULL},
  {djb_atm_form_initer_prompt_pin      , djb_atm_form_handler_prompt_pin      , NULL},
  {djb_atm_form_initer_show_pin_failed , djb_atm_form_handler_show_pin_failed , NULL},
  {djb_atm_form_initer_prompt_option   , djb_atm_form_handler_prompt_option   , NULL},
  {NULL                                , djb_atm_form_handler_prompt_take_card, NULL},
  {djb_atm_form_initer_show_balance    , djb_atm_form_handler_show_balance    , NULL},
  {djb_atm_form_initer_error_balance   , djb_atm_form_handler_show_balance    ,
                                         djb_atm_form_freer_prompt_withdraw         },
  {djb_atm_form_initer_prompt_withdraw , djb_atm_form_handler_prompt_withdraw , NULL},
  {djb_atm_form_initer_prompt_withdraw , djb_atm_form_handler_prompt_wdraw_rcp, NULL},
  {djb_atm_form_initer_show_receipt    , djb_atm_form_handler_show_receipt    , NULL},
  {NULL                                , djb_atm_form_handler_prompt_take_cash,
                                         djb_atm_form_freer_prompt_withdraw         },
  {NULL                                , djb_atm_form_handler_prompt_shutdown , NULL},
};

static const double djb_atm_withdraw_hotkey_amounts[8] =
  {10, 20, 30, 50, 100, 150, 200, 250};

static void
djb_atm_class_init (DjbAtmClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->constructed = djb_atm_constructed;
  object_class->dispose     = djb_atm_dispose;
  object_class->finalize    = djb_atm_finalize;

  object_class->get_property = djb_atm_get_property;
  object_class->set_property = djb_atm_set_property;

  /**
   * DjbAtm:database:
   *
   * the database containing banking data. You can think of this as representing
   * 2-way communication with the bank: account details are read from the
   * database, and withdrawals or failed PIN attempts cause updates to be
   * written back to it.
   */
  djb_atm_properties[DJB_ATM_PROPERTY_DATABASE] =
    g_param_spec_string ("database", "Database",
                         "the database containing banking data", NULL,
                         G_PARAM_STATIC_STRINGS |
                           G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE);

  /**
   * DjbAtm:state:
   *
   * the state of the ATM. See #DjbAtmState and djb_atm_get_state().
   */
  djb_atm_properties[DJB_ATM_PROPERTY_STATE] =
    g_param_spec_enum ("state", "State", "the state of the ATM",
                       DJB_TYPE_ATM_STATE, DJB_ATM_STATE_OFF,
                       G_PARAM_STATIC_STRINGS |
                         G_PARAM_READABLE | G_PARAM_EXPLICIT_NOTIFY);

  g_object_class_install_properties (object_class, DJB_ATM_N_PROPERTIES,
                                     djb_atm_properties);
}

static void
djb_atm_init (DjbAtm *atm)
{
  g_assert (djb_atm_get_resource ());
  GtkBuilder *builder = gtk_builder_new_from_resource ("/org/djb/atm/window.ui");

  atm->window = GTK_WINDOW (gtk_builder_get_object (builder, "window"));
  atm->bin_form = GTK_BIN (gtk_builder_get_object (builder, "bin_form"));

  g_assert (DJB_ATM_BUTTON_HOTKEY_A == 0);
  g_assert (DJB_ATM_BUTTON_HOTKEY_H == G_N_ELEMENTS (atm->hotkey_labels) - 1);

  for (unsigned i = DJB_ATM_BUTTON_HOTKEY_A; i <= DJB_ATM_BUTTON_HOTKEY_H; ++i)
    {
      char *label_id = g_strdup_printf ("hotkey_label_%c", 'a' + i);
      atm->hotkey_labels[i] = GTK_LABEL (gtk_builder_get_object (builder, label_id));
      g_free (label_id);
    }

  static const char *f_button_ids[] = {
    "hotkey_button_a",
    "hotkey_button_b",
    "hotkey_button_c",
    "hotkey_button_d",
    "hotkey_button_e",
    "hotkey_button_f",
    "hotkey_button_g",
    "hotkey_button_h",
    "number_button_0",
    "number_button_1",
    "number_button_2",
    "number_button_3",
    "number_button_4",
    "number_button_5",
    "number_button_6",
    "number_button_7",
    "number_button_8",
    "number_button_9",
    "function_button_cancel",
    "function_button_clear",
    "function_button_enter",
  };

  for (size_t i = 0; i < G_N_ELEMENTS (f_button_ids); ++i)
    {
      GObject *button = gtk_builder_get_object (builder, f_button_ids[i]);

      DjbAtmButtonContext *context = g_new (DjbAtmButtonContext, 1);
      context->atm = atm;
      context->button_id  = (DjbAtmButtonId) i;
      g_signal_connect_data (button, "clicked",
                             G_CALLBACK (djb_atm_on_button_clicked), context,
                             (GClosureNotify) g_free, G_CONNECT_SWAPPED);
    }

  g_signal_connect_swapped (atm->window, "delete-event",
                            G_CALLBACK (djb_atm_on_window_delete), atm);
  gtk_widget_show_all (GTK_WIDGET (atm->window));

  g_object_unref (builder);
}

/**
 * djb_atm_new:
 *
 * Create a new DjbAtm using the given @database as its persistent storage.
 *
 * @database: The filename of the database to read/write banking data from/to.
 * Returns: a new DjbAtm
 */
DjbAtm *
djb_atm_new (const char *database)
{
  return DJB_ATM (g_object_new (DJB_TYPE_ATM, "database", database, NULL));
}

static void
djb_atm_constructed (GObject *object)
{
  G_OBJECT_CLASS (djb_atm_parent_class)->constructed (object);

  DjbAtm *atm = DJB_ATM (object);

  char *title = g_strdup_printf ("DjbAtm (%s)", atm->database);
  gtk_window_set_title (atm->window, title);
  g_free (title);
}

static void
djb_atm_dispose (GObject *object)
{
  DjbAtm *atm = DJB_ATM (object);

  if (atm->window != NULL)
    {
      gtk_widget_destroy (GTK_WIDGET (atm->window));
      atm->window = NULL;
    }

  G_OBJECT_CLASS (djb_atm_parent_class)->dispose (object);
}

static void
djb_atm_finalize (GObject *object)
{
  DjbAtm *atm = DJB_ATM (object);

  g_free (atm->database);
  sqlite3_close (atm->sqlite);

  G_OBJECT_CLASS (djb_atm_parent_class)->finalize (object);
}

static void
djb_atm_get_property (GObject *object, unsigned property_id,
                      GValue *value, GParamSpec *pspec)
{
  DjbAtm *atm = DJB_ATM (object);

  switch (property_id)
  {
  case DJB_ATM_PROPERTY_DATABASE:
    g_value_set_string (value, djb_atm_get_database (atm));
    break;

  case DJB_ATM_PROPERTY_STATE:
    g_value_set_enum (value, djb_atm_get_state (atm));
    break;

  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    break;
  }
}

/**
 * djb_atm_get_database:
 *
 * @atm: a DjbAtm
 *
 * Returns: the value of the #DjbAtm:database property
 */
const char *
djb_atm_get_database (DjbAtm *atm)
{
  g_return_val_if_fail (DJB_IS_ATM (atm), NULL);

  return atm->database;
}

/**
 * djb_atm_get_state:
 *
 * @atm: a DjbAtm
 *
 * Returns: the value of the #DjbAtm:state property
 */
DjbAtmState
djb_atm_get_state (DjbAtm *atm)
{
  g_return_val_if_fail (DJB_IS_ATM (atm), DJB_ATM_STATE_OFF);

  return atm->state;
}

// TODO: Shouldn't this be a (read-only) property?
/**
 * djb_atm_get_window:
 *
 * @atm: a DjbAtm
 *
 * The window can only be closed once a user with manager privileges has shut
 * down the ATM to #DJB_ATM_STATE_OFF. Closing the window will in fact only
 * hide it, so you should connect to its GtkWidget::hide signal if you want to
 * destroy the ATM in response to its window being ‘closed’ in this manner.
 *
 * Returns: (transfer none): the GtkWindow for @atm
 */
GtkWindow *
djb_atm_get_window (DjbAtm *atm)
{
  g_return_val_if_fail (DJB_IS_ATM (atm), NULL);

  return atm->window;
}

static void
djb_atm_set_property (GObject *object, unsigned property_id,
                      const GValue *value, GParamSpec *pspec)
{
  DjbAtm *atm = DJB_ATM (object);

  switch (property_id)
  {
  case DJB_ATM_PROPERTY_DATABASE:
    djb_atm_set_database (atm, g_value_get_string (value));
    break;

  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    break;
  }
}

static void
djb_atm_set_database (DjbAtm *atm, const char *database)
{
  g_assert (atm->database == NULL);

  g_assert (database != NULL);
  g_assert (strlen (database) > 0);

  int result = sqlite3_open_v2 (database, &atm->sqlite, SQLITE_OPEN_READWRITE, 0);
  if (result != SQLITE_OK)
    {
      g_error ("error opening database \"%s\": \"%s\"",
               database, sqlite3_errmsg (atm->sqlite));

      sqlite3_close (atm->sqlite);
      abort ();
    }

  atm->database = g_strdup (database);
}

static void
djb_atm_set_state (DjbAtm *atm, DjbAtmState state)
{
  if (state == atm->state)
    return;

  GtkWidget *form = gtk_bin_get_child (atm->bin_form);
  if (form != NULL)
    {
      DjbAtmFormFreer form_freer = djb_atm_form_functions[atm->state].freer;
      if (form_freer != NULL)
        form_freer (atm);

      gtk_container_remove (GTK_CONTAINER (atm->bin_form), form);

      for (unsigned i = 8; i--;)
        djb_atm_set_hotkey_label (atm, i, "");
    }

  atm->state = state;

  GtkStyleContext *style_context = gtk_widget_get_style_context (GTK_WIDGET (atm->window));
  if (atm->state == DJB_ATM_STATE_OFF)
    gtk_style_context_remove_class (style_context, "on");
  else
    gtk_style_context_add_class (style_context, "on");

  gtk_window_set_deletable (atm->window, atm->state == DJB_ATM_STATE_OFF);

  char *path = g_strdup_printf ("/org/djb/atm/form%i.ui", atm->state);
  GtkBuilder *builder = gtk_builder_new ();

  g_type_ensure (DJB_TYPE_ATM_HOTKEY_SET);
  if (gtk_builder_add_from_resource (builder, path, NULL))
    {
      DjbAtmFormIniter form_initer = djb_atm_form_functions[atm->state].initer;
      if (form_initer != NULL)
        form_initer (atm, builder);

      GtkWidget *form = GTK_WIDGET (gtk_builder_get_object (builder, "form"));
      if (form != NULL)
        {
          gtk_widget_set_valign (form, GTK_ALIGN_CENTER);
          gtk_widget_show_all (form);
          gtk_container_add (GTK_CONTAINER (atm->bin_form), form);
        }

      GObject *set = gtk_builder_get_object (builder, "hotkey_set");
      if (set != NULL)
        {
          for (unsigned i = 8; i--;)
            {
              char *property_name = g_strdup_printf ("label-%c", 'a' + i);
              char *label;

              g_object_get (set, property_name, &label, NULL);
              djb_atm_set_hotkey_label (atm, i, label);

              g_free (property_name);
              g_free (label);
            }
        }
    }

  g_object_notify_by_pspec (G_OBJECT (atm),
                            djb_atm_properties[DJB_ATM_PROPERTY_STATE]);

  g_free (path);
  g_object_unref (builder);
}

static void
djb_atm_form_initer_off (DjbAtm *atm, GtkBuilder *builder)
{
  djb_atm_clear_login (atm);
}

static void
djb_atm_form_initer_show_welcome (DjbAtm *atm, GtkBuilder *builder)
{
  djb_atm_clear_login (atm);
}

static void
djb_atm_form_initer_prompt_account (DjbAtm *atm, GtkBuilder *builder)
{
  atm->form_context = gtk_builder_get_object (builder, "label_account");
  djb_atm_form_handler_prompt_account_clear (atm);
}

static void
djb_atm_form_initer_prompt_pin (DjbAtm *atm, GtkBuilder *builder)
{
  atm->form_context = gtk_builder_get_object (builder, "label_pin");
  djb_atm_form_handler_prompt_pin_clear (atm);
}

static void
djb_atm_form_initer_show_pin_failed (DjbAtm *atm, GtkBuilder *builder)
{
  GtkLabel *label = GTK_LABEL (gtk_builder_get_object (builder, "label_tries"));
  unsigned n_pin_retries = DJB_ATM_PIN_RETRIES - atm->n_pin_failures;
  char *text = g_strdup_printf ("You get %u more %s...",
                                n_pin_retries, n_pin_retries > 1 ? "tries" : "try");
  gtk_label_set_label (label, text);
  g_free (text);
}

static void
djb_atm_form_initer_prompt_option (DjbAtm *atm, GtkBuilder *builder)
{
  GtkLabel *label = atm->hotkey_labels[DJB_ATM_BUTTON_HOTKEY_H];
  gtk_widget_set_visible (GTK_WIDGET (label), atm->is_manager);
}

static void
djb_atm_form_initer_show_balance (DjbAtm *atm, GtkBuilder *builder)
{
  djb_atm_form_initer_show_balance_impl (atm, builder, "form");
}

static void
djb_atm_form_initer_error_balance (DjbAtm *atm, GtkBuilder *builder)
{
  djb_atm_form_initer_show_balance_impl (atm, builder, "label_main");

  DjbAtmPromptWithdrawContext *context = atm->form_context;
  char *text = g_strdup_printf ("You can't withdraw "
                                DJB_ATM_CURRENCY_SYMBOL "%.0f.\n\n", context->amount);
  GtkLabel *label = GTK_LABEL (gtk_builder_get_object (builder, "label"));
  gtk_label_set_label (label, text);
  g_free (text);
}

static void
djb_atm_form_initer_prompt_withdraw (DjbAtm *atm, GtkBuilder *builder)
{
  for (unsigned i = 8; i--;)
    {
      char *text = g_strdup_printf ("%s%.0f", DJB_ATM_CURRENCY_SYMBOL,
                                    djb_atm_withdraw_hotkey_amounts[i]);
      djb_atm_set_hotkey_label (atm, DJB_ATM_BUTTON_HOTKEY_A + i, text);
      g_free (text);
    }

  DjbAtmPromptWithdrawContext *context = g_new0 (DjbAtmPromptWithdrawContext, 1);
  context->label = GTK_LABEL (gtk_builder_get_object (builder, "label_amount"));
  atm->form_context = context;

  djb_atm_form_handler_prompt_withdraw_clear (atm);
}

static void
djb_atm_form_initer_show_receipt (DjbAtm *atm, GtkBuilder *builder)
{
  double balance, withdrawn, available;
  djb_atm_get_balances (atm, &balance, &withdrawn, &available);

  DjbAtmPromptWithdrawContext *context = atm->form_context;

  char *text = g_strdup_printf (
    "You withdrew " DJB_ATM_CURRENCY_SYMBOL "%.2f.\n"
    "\n"
    "Your balance is:\n"
    "     " DJB_ATM_CURRENCY_SYMBOL "%.2f\n"
    "\n"
    "Today you can withdraw:\n"
    "     " DJB_ATM_CURRENCY_SYMBOL "%.2f\n",

    context->amount, balance, available);

  GtkLabel *label = GTK_LABEL (gtk_builder_get_object (builder, "form"));
  gtk_label_set_label (label, text);
  g_free (text);
}

static gboolean
djb_atm_form_handler_off (DjbAtm *atm, DjbAtmButtonId button_id)
{
  if (button_id == DJB_ATM_BUTTON_ENTER)
    {
      djb_atm_set_state (atm, DJB_ATM_STATE_SHOW_WELCOME);
      return TRUE;
    }

  return FALSE;
}

static gboolean
djb_atm_form_handler_show_welcome (DjbAtm *atm, DjbAtmButtonId button_id)
{
  if (button_id == DJB_ATM_BUTTON_HOTKEY_H)
    {
      djb_atm_set_state (atm, DJB_ATM_STATE_PROMPT_ACCOUNT);
      return TRUE;
    }

  return FALSE;
}

static gboolean
djb_atm_form_handler_prompt_account (DjbAtm *atm, DjbAtmButtonId button_id)
{
  switch (button_id)
  {
  case DJB_ATM_BUTTON_0:
  case DJB_ATM_BUTTON_1:
  case DJB_ATM_BUTTON_2:
  case DJB_ATM_BUTTON_3:
  case DJB_ATM_BUTTON_4:
  case DJB_ATM_BUTTON_5:
  case DJB_ATM_BUTTON_6:
  case DJB_ATM_BUTTON_7:
  case DJB_ATM_BUTTON_8:
  case DJB_ATM_BUTTON_9:
    {
      unsigned number = button_id - DJB_ATM_BUTTON_0;
      return djb_atm_form_handler_prompt_account_append (atm, number);
    }

  case DJB_ATM_BUTTON_CANCEL:
    djb_atm_set_state (atm, DJB_ATM_STATE_SHOW_WELCOME);
    return TRUE;

  case DJB_ATM_BUTTON_CLEAR:
    djb_atm_form_handler_prompt_account_clear (atm);
    return TRUE;

  case DJB_ATM_BUTTON_ENTER:
    return djb_atm_form_handler_prompt_account_enter (atm);

  default:
    return FALSE;
  }
}

static gboolean
djb_atm_form_handler_show_no_account (DjbAtm *atm, DjbAtmButtonId button_id)
{
  switch (button_id)
  {
  case DJB_ATM_BUTTON_HOTKEY_H:
    djb_atm_set_state (atm, DJB_ATM_STATE_PROMPT_ACCOUNT);
    return TRUE;

  default:
    return FALSE;
  }
}

static gboolean
djb_atm_form_handler_show_locked (DjbAtm *atm, DjbAtmButtonId button_id)
{
  switch (button_id)
  {
  case DJB_ATM_BUTTON_HOTKEY_H:
    djb_atm_set_state (atm, DJB_ATM_STATE_SHOW_WELCOME);
    return TRUE;

  default:
    return FALSE;
  }
}

static gboolean
djb_atm_form_handler_prompt_pin (DjbAtm *atm, DjbAtmButtonId button_id)
{
  switch (button_id)
  {
  case DJB_ATM_BUTTON_0:
  case DJB_ATM_BUTTON_1:
  case DJB_ATM_BUTTON_2:
  case DJB_ATM_BUTTON_3:
  case DJB_ATM_BUTTON_4:
  case DJB_ATM_BUTTON_5:
  case DJB_ATM_BUTTON_6:
  case DJB_ATM_BUTTON_7:
  case DJB_ATM_BUTTON_8:
  case DJB_ATM_BUTTON_9:
    {
      unsigned number = button_id - DJB_ATM_BUTTON_0;
      return djb_atm_form_handler_prompt_pin_append (atm, number);
    }

  case DJB_ATM_BUTTON_CANCEL:
    djb_atm_set_state (atm, DJB_ATM_STATE_PROMPT_REMOVE_CARD);
    return TRUE;

  case DJB_ATM_BUTTON_CLEAR:
    djb_atm_form_handler_prompt_pin_clear (atm);
    return TRUE;

  case DJB_ATM_BUTTON_ENTER:
    return djb_atm_form_handler_prompt_pin_enter (atm);

  default:
    return FALSE;
  }
}

static gboolean
djb_atm_form_handler_show_pin_failed (DjbAtm *atm, DjbAtmButtonId button_id)
{
  switch (button_id)
  {
  case DJB_ATM_BUTTON_HOTKEY_H:
    djb_atm_set_state (atm, DJB_ATM_STATE_PROMPT_PIN);
    return TRUE;

  default:
    return FALSE;
  }
}

static gboolean
djb_atm_form_handler_prompt_option (DjbAtm *atm, DjbAtmButtonId button_id)
{
  switch (button_id)
  {
  case DJB_ATM_BUTTON_HOTKEY_A:
    djb_atm_set_state (atm, DJB_ATM_STATE_SHOW_BALANCE);
    return TRUE;

  case DJB_ATM_BUTTON_HOTKEY_C:
    djb_atm_set_state (atm, DJB_ATM_STATE_PROMPT_WITHDRAW);
    return TRUE;

  case DJB_ATM_BUTTON_HOTKEY_D:
    djb_atm_set_state (atm, DJB_ATM_STATE_PROMPT_WITHDRAW_RECEIPT);
    return TRUE;

  case DJB_ATM_BUTTON_HOTKEY_H:
    if (atm->is_manager)
      djb_atm_set_state (atm, DJB_ATM_STATE_PROMPT_SHUTDOWN);

    return atm->is_manager;

  case DJB_ATM_BUTTON_CANCEL:
    djb_atm_set_state (atm, DJB_ATM_STATE_PROMPT_REMOVE_CARD);
    return TRUE;

  default:
    return FALSE;
  }
}

static gboolean
djb_atm_form_handler_prompt_take_card (DjbAtm *atm, DjbAtmButtonId button_id)
{
  switch (button_id)
  {
  case DJB_ATM_BUTTON_HOTKEY_H:
    djb_atm_set_state (atm, DJB_ATM_STATE_SHOW_WELCOME);
    return TRUE;

  default:
    return FALSE;
  }
}

static gboolean
djb_atm_form_handler_show_balance (DjbAtm *atm, DjbAtmButtonId button_id)
{
  switch (button_id)
  {
  case DJB_ATM_BUTTON_HOTKEY_A:
    djb_atm_set_state (atm, DJB_ATM_STATE_PROMPT_OPTION);
    return TRUE;

  case DJB_ATM_BUTTON_CANCEL:
    djb_atm_set_state (atm, DJB_ATM_STATE_PROMPT_REMOVE_CARD);
    return TRUE;

  default:
    return FALSE;
  }
}

static gboolean
djb_atm_form_handler_prompt_withdraw (DjbAtm *atm, DjbAtmButtonId button_id)
{
  return djb_atm_form_handler_prompt_withdraw_impl (atm, button_id, FALSE);
}

static gboolean
djb_atm_form_handler_prompt_wdraw_rcp (DjbAtm *atm, DjbAtmButtonId button_id)
{
  return djb_atm_form_handler_prompt_withdraw_impl (atm, button_id, TRUE);
}

static gboolean
djb_atm_form_handler_show_receipt (DjbAtm *atm, DjbAtmButtonId button_id)
{
  switch (button_id)
  {
  case DJB_ATM_BUTTON_HOTKEY_H:
  case DJB_ATM_BUTTON_CANCEL:
    djb_atm_set_state (atm, DJB_ATM_STATE_PROMPT_REMOVE_CASH);
    return TRUE;

  default:
    return FALSE;
  }
}

static gboolean
djb_atm_form_handler_prompt_take_cash (DjbAtm *atm, DjbAtmButtonId button_id)
{
  switch (button_id)
  {
  case DJB_ATM_BUTTON_HOTKEY_H:
    djb_atm_set_state (atm, DJB_ATM_STATE_SHOW_WELCOME);
    return TRUE;

  default:
    return FALSE;
  }
}

static gboolean
djb_atm_form_handler_prompt_shutdown (DjbAtm *atm, DjbAtmButtonId button_id)
{
  switch (button_id)
  {
  case DJB_ATM_BUTTON_HOTKEY_C:
    djb_atm_set_state (atm, DJB_ATM_STATE_PROMPT_OPTION);
    return TRUE;

  case DJB_ATM_BUTTON_HOTKEY_G:
    djb_atm_set_state (atm, DJB_ATM_STATE_OFF);
    return TRUE;

  default:
    return FALSE;
  }
}

static void
djb_atm_form_freer_prompt_withdraw (DjbAtm *atm)
{
  DjbAtmPromptWithdrawContext *context = atm->form_context;
  g_assert (context != NULL);
  g_free (context);
}

static void
djb_atm_form_handler_prompt_account_clear (DjbAtm *atm)
{
  memset (atm->account, 0, sizeof (atm->account));

  GtkLabel *label_account = GTK_LABEL (atm->form_context);
  gtk_label_set_label (label_account, "_");
}

static gboolean
djb_atm_form_handler_prompt_account_append (DjbAtm *atm, unsigned number)
{
  g_assert (number <= 9);

  size_t have_account_digits = strlen (atm->account);
  g_assert (have_account_digits < sizeof (atm->account));
  gboolean have_account = have_account_digits == DJB_ATM_ACCOUNT_DIGITS;

  if (have_account)
    return FALSE; // can't accept more input: press Enter instead!

  char digit = (char)('0' + number);
  atm->account[have_account_digits] = digit;
  ++have_account_digits;

  char text[DJB_ATM_ACCOUNT_DIGITS + 1] = {0,};
  strcpy (text, atm->account);
  if (have_account_digits < DJB_ATM_ACCOUNT_DIGITS)
    text[have_account_digits++] = '_';

  GtkLabel *label_account = GTK_LABEL (atm->form_context);
  gtk_label_set_label (label_account, text);

  return TRUE;
}

static gboolean
djb_atm_form_handler_prompt_account_enter (DjbAtm *atm)
{
  size_t have_account_digits = strlen (atm->account);
  gboolean have_account = have_account_digits == DJB_ATM_ACCOUNT_DIGITS;

  if (!have_account)
    return FALSE;

  sqlite3_stmt *stmt;
  sqlite3_prepare_v2 (atm->sqlite,
    "select status "
    "from Account "
    "where number = ?;",
    -1, &stmt, NULL);
  sqlite3_bind_text (stmt, 1, atm->account, DJB_ATM_ACCOUNT_DIGITS,
                     SQLITE_STATIC);

  int result = djb_atm_sqlite3_step (atm, stmt, TRUE);
  gboolean account_exists = result == SQLITE_ROW;

  if (account_exists)
    {
      DjbAtmAccountStatus status = sqlite3_column_int (stmt, 0);

      switch (status)
      {
      case DJB_ATM_ACCOUNT_STATUS_ENABLED:
        djb_atm_set_state (atm, DJB_ATM_STATE_PROMPT_PIN);
        break;

      case DJB_ATM_ACCOUNT_STATUS_LOCKED:
      case DJB_ATM_ACCOUNT_STATUS_DISABLED:
        djb_atm_set_state (atm, DJB_ATM_STATE_SHOW_LOCKED);
        break;
      }
    }
  else
    {
      djb_atm_set_state (atm, DJB_ATM_STATE_SHOW_ACCOUNT_NONEXISTENT);
    }

  sqlite3_finalize (stmt);

  return TRUE;
}

static void
djb_atm_form_handler_prompt_pin_clear (DjbAtm *atm)
{
  memset (atm->pin, 0, sizeof (atm->pin));

  GtkLabel *label_pin = GTK_LABEL (atm->form_context);
  gtk_label_set_label (label_pin, "_");
}

static gboolean
djb_atm_form_handler_prompt_pin_append (DjbAtm *atm, unsigned number)
{
  g_assert (number <= 9);

  size_t have_pin_digits = strlen (atm->pin);
  g_assert (have_pin_digits < sizeof (atm->pin));
  gboolean have_pin = have_pin_digits == DJB_ATM_PIN_DIGITS;

  if (have_pin)
    return FALSE; // can't accept more input: press Enter instead!

  char digit = (char)('0' + number);
  atm->pin[have_pin_digits] = digit;
  ++have_pin_digits;

  char text[DJB_ATM_PIN_DIGITS + 1] = {0,};
  memset (text, '*', have_pin_digits);
  if (have_pin_digits < DJB_ATM_PIN_DIGITS)
    text[have_pin_digits] = '_';

  GtkLabel *label_pin = GTK_LABEL (atm->form_context);
  gtk_label_set_label (label_pin, text);

  return TRUE;
}

static gboolean
djb_atm_form_handler_prompt_pin_enter (DjbAtm *atm)
{
  size_t have_pin_digits = strlen (atm->pin);
  gboolean have_pin = have_pin_digits == DJB_ATM_PIN_DIGITS;
  if (!have_pin)
    return FALSE;

  g_assert (strlen (atm->account) == DJB_ATM_ACCOUNT_DIGITS);

  sqlite3_stmt *stmt;
  sqlite3_prepare_v2 (atm->sqlite,
    "select is_manager "
    "from Account "
    "where "
    "  number = ? and "
    "  pin    = ?;",
    -1, &stmt, NULL);
  sqlite3_bind_text (stmt, 1, atm->account, DJB_ATM_ACCOUNT_DIGITS,
                     SQLITE_STATIC);
  sqlite3_bind_text (stmt, 2, atm->pin, DJB_ATM_PIN_DIGITS,
                     SQLITE_STATIC);

  int result = djb_atm_sqlite3_step (atm, stmt, TRUE);
  gboolean authenticated = result == SQLITE_ROW;
  if (authenticated)
    atm->is_manager = sqlite3_column_int (stmt, 0);

  sqlite3_finalize (stmt);

  if (authenticated)
    {
      djb_atm_set_state (atm, DJB_ATM_STATE_PROMPT_OPTION);
      return TRUE;
    }

  ++atm->n_pin_failures;

  if (atm->n_pin_failures < DJB_ATM_PIN_RETRIES)
    {
      djb_atm_set_state (atm, DJB_ATM_STATE_SHOW_PIN_FAILED);
      return TRUE;
    }

  sqlite3_prepare_v2 (atm->sqlite,
    "update Account "
    "set status = ? "
    "where number = ?;",
    -1, &stmt, NULL);
  sqlite3_bind_int (stmt, 1, DJB_ATM_ACCOUNT_STATUS_LOCKED);
  sqlite3_bind_text (stmt, 2, atm->account, DJB_ATM_ACCOUNT_DIGITS,
                     SQLITE_STATIC);

  djb_atm_sqlite3_step (atm, stmt, FALSE);
  sqlite3_finalize (stmt);

  djb_atm_set_state (atm, DJB_ATM_STATE_SHOW_LOCKED);
  return TRUE;
}

static void
djb_atm_form_initer_show_balance_impl (DjbAtm *atm, GtkBuilder *builder,
                                       const char *label_main_id)
{
  double balance, withdrawn, available;
  djb_atm_get_balances (atm, &balance, &withdrawn, &available);

  char *text = g_strdup_printf (
    "Your balance is:\n"
    "     " DJB_ATM_CURRENCY_SYMBOL "%.2f\n"
    "\n"
    "Today you've withdrawn:\n"
    "     " DJB_ATM_CURRENCY_SYMBOL "%.2f\n"
    "\n"
    "Today you can withdraw:\n"
    "     " DJB_ATM_CURRENCY_SYMBOL "%.2f\n"
    "in units of " DJB_ATM_CURRENCY_SYMBOL "%.2f",

    balance, withdrawn, available, DJB_ATM_WITHDRAW_MULTIPLE);

  GtkLabel *label = GTK_LABEL (gtk_builder_get_object (builder, label_main_id));
  gtk_label_set_label (label, text);
  g_free (text);
}

static void
djb_atm_form_handler_prompt_withdraw_clear (DjbAtm *atm)
{
  DjbAtmPromptWithdrawContext *context = atm->form_context;

  memset (context->digits, 0, sizeof (context->digits));
  gtk_label_set_label (GTK_LABEL (context->label), DJB_ATM_CURRENCY_SYMBOL "_");
}

static gboolean
djb_atm_form_handler_prompt_withdraw_append (DjbAtm *atm, unsigned number)
{
  DjbAtmPromptWithdrawContext *context = atm->form_context;

  g_assert (number <= 9);

  size_t have_amount_digits = strlen (context->digits);
  g_assert (have_amount_digits < sizeof (context->digits));
  gboolean is_full = have_amount_digits == DJB_ATM_WITHDRAW_DIGITS;

  if (is_full)
    return FALSE; // can't accept more input: press Enter instead!

  char digit = (char)('0' + number);
  context->digits[have_amount_digits] = digit;
  ++have_amount_digits;

  char *text = g_strdup_printf ("%s%s%s",
                                DJB_ATM_CURRENCY_SYMBOL,
                                context->digits,
                                have_amount_digits < DJB_ATM_WITHDRAW_DIGITS ? "_" : "");
  gtk_label_set_label (GTK_LABEL (context->label), text);
  g_free (text);

  return TRUE;
}

static gboolean
djb_atm_form_handler_prompt_withdraw_enter (DjbAtm *atm, gboolean with_receipt)
{
  DjbAtmPromptWithdrawContext *context = atm->form_context;

  g_assert (strlen (context->digits) > 0);
  double amount = atof (context->digits);
  g_assert (amount > 0);

  double amount_rounded = floor (amount / DJB_ATM_WITHDRAW_MULTIPLE)
                          * DJB_ATM_WITHDRAW_MULTIPLE;
  if (amount != amount_rounded)
    {
      context->amount = amount;
      djb_atm_set_state (atm, DJB_ATM_STATE_SHOW_BALANCE_AS_ERROR);
      return FALSE;
    }

  djb_atm_form_handler_prompt_withdraw_do (atm, amount, with_receipt);
  return TRUE;
}

static gboolean
djb_atm_form_handler_prompt_withdraw_impl (DjbAtm *atm, DjbAtmButtonId button_id,
                                           gboolean with_receipt)
{
  DjbAtmPromptWithdrawContext *context = atm->form_context;
  gboolean have_entry = strlen (context->digits) > 0;

  switch (button_id)
  {
  case DJB_ATM_BUTTON_HOTKEY_A:
  case DJB_ATM_BUTTON_HOTKEY_B:
  case DJB_ATM_BUTTON_HOTKEY_C:
  case DJB_ATM_BUTTON_HOTKEY_D:
  case DJB_ATM_BUTTON_HOTKEY_E:
  case DJB_ATM_BUTTON_HOTKEY_F:
  case DJB_ATM_BUTTON_HOTKEY_G:
  case DJB_ATM_BUTTON_HOTKEY_H:
    {
      double amount = djb_atm_withdraw_hotkey_amounts
                      [button_id - DJB_ATM_BUTTON_HOTKEY_A];
      djb_atm_form_handler_prompt_withdraw_do (atm, amount, with_receipt);
      return TRUE;
    }

  case DJB_ATM_BUTTON_0:
    if (!have_entry)
      return FALSE;
    /* else fall through */

  case DJB_ATM_BUTTON_1:
  case DJB_ATM_BUTTON_2:
  case DJB_ATM_BUTTON_3:
  case DJB_ATM_BUTTON_4:
  case DJB_ATM_BUTTON_5:
  case DJB_ATM_BUTTON_6:
  case DJB_ATM_BUTTON_7:
  case DJB_ATM_BUTTON_8:
  case DJB_ATM_BUTTON_9:
    {
      unsigned number = button_id - DJB_ATM_BUTTON_0;
      return djb_atm_form_handler_prompt_withdraw_append (atm, number);
    }

  case DJB_ATM_BUTTON_CANCEL:
    djb_atm_set_state (atm, DJB_ATM_STATE_PROMPT_REMOVE_CARD);
    return TRUE;

  case DJB_ATM_BUTTON_CLEAR:
    djb_atm_form_handler_prompt_withdraw_clear (atm);
    return TRUE;

  case DJB_ATM_BUTTON_ENTER:
    if (have_entry)
      return djb_atm_form_handler_prompt_withdraw_enter (atm, with_receipt);
    /* else fall through */

  default:
    return FALSE;
  }
}

static void
djb_atm_form_handler_prompt_withdraw_do (DjbAtm *atm, double amount,
                                         gboolean with_receipt)
{
  double balance, withdrawn, available;
  djb_atm_get_balances (atm, &balance, &withdrawn, &available);

  if (amount > available)
    {
      DjbAtmPromptWithdrawContext *context = atm->form_context;
      context->amount = amount;

      djb_atm_set_state (atm, DJB_ATM_STATE_SHOW_BALANCE_AS_ERROR);
      return;
    }

  sqlite3_stmt *stmt;
  sqlite3_prepare_v2 (atm->sqlite,
    "update Account "
    "set balance = balance - ? "
    "where number = ?;",
    -1, &stmt, NULL);
  sqlite3_bind_text (stmt, 2, atm->account, DJB_ATM_ACCOUNT_DIGITS,
                     SQLITE_STATIC);
  sqlite3_bind_double (stmt, 1, amount);

  djb_atm_sqlite3_step (atm, stmt, FALSE);
  sqlite3_finalize (stmt);

  sqlite3_prepare_v2 (atm->sqlite,
    "insert into Withdrawal (account, amount) "
    "values (?, ?);",
    -1, &stmt, NULL);
  sqlite3_bind_text (stmt, 1, atm->account, DJB_ATM_ACCOUNT_DIGITS,
                     SQLITE_STATIC);
  sqlite3_bind_double (stmt, 2, amount);

  djb_atm_sqlite3_step (atm, stmt, FALSE);
  sqlite3_finalize (stmt);

  if (with_receipt)
    {
      DjbAtmPromptWithdrawContext *context = atm->form_context;
      context->amount = amount;

      djb_atm_set_state (atm, DJB_ATM_STATE_SHOW_RECEIPT);
    }
  else
    {
      djb_atm_set_state (atm, DJB_ATM_STATE_PROMPT_REMOVE_CASH);
    }
}

static void
djb_atm_beep (DjbAtm *atm)
{
  gtk_widget_error_bell (GTK_WIDGET (atm->window));
}

static void
djb_atm_clear_login (DjbAtm *atm)
{
  // Clear the previous user's details, in case of hilarious security breach
  memset (atm->account, 0, sizeof (atm->account));
  memset (atm->pin    , 0, sizeof (atm->pin    ));

  atm->is_manager = FALSE;
  atm->n_pin_failures = 0;
}

static void
djb_atm_get_balances (DjbAtm *atm, double *out_balance, double *out_withdrawn,
                      double *out_available)
{
  sqlite3_stmt *stmt;
  sqlite3_prepare_v2 (atm->sqlite,
    "select "
    "  Account.balance, "
    "  ifnull (sum (Withdrawal.amount), 0) "
    "from Account "
    "left join Withdrawal on "
    "  Account.number = Withdrawal.account and "
    "  Withdrawal.timestamp >= date ('now') "
    "where Account.number = ? "
    "group by Account.balance;",
    -1, &stmt, NULL);
  sqlite3_bind_text (stmt, 1, atm->account, DJB_ATM_ACCOUNT_DIGITS,
                     SQLITE_STATIC);

  int result = djb_atm_sqlite3_step (atm, stmt, TRUE);
  g_assert (result == SQLITE_ROW);
  double balance   = sqlite3_column_double (stmt, 0);
  double withdrawn = sqlite3_column_double (stmt, 1);
  double available = DJB_ATM_WITHDRAW_DAILY - withdrawn;
  sqlite3_finalize (stmt);

  if (balance < available)
    available = balance;

  if (available < 0) // Accommodate overdrafts!
    available = 0;
  else // Round down to the lowest note that can be given out
    available = floor (available / DJB_ATM_WITHDRAW_MULTIPLE)
                      * DJB_ATM_WITHDRAW_MULTIPLE;

  *out_balance   = balance;
  *out_withdrawn = withdrawn;
  *out_available = available;
}


static void
djb_atm_on_button_clicked (DjbAtmButtonContext *context)
{
  djb_atm_push_button (context->atm, context->button_id);
}

static gboolean
djb_atm_on_window_delete (DjbAtm *atm)
{
  // can only be closed by manager. I only hide to avoid leaving ATM half-broken
  if (atm->state == DJB_ATM_STATE_OFF)
    gtk_widget_hide (GTK_WIDGET (atm->window));
  else // Otherwise, warn the user that they can't close the window yet.
    djb_atm_beep (atm);

  return TRUE;
}

static void
djb_atm_set_hotkey_label (DjbAtm *atm, DjbAtmButtonId button_id,
                          const char *text)
{
  g_assert (button_id >= DJB_ATM_BUTTON_HOTKEY_A);
  g_assert (button_id <= DJB_ATM_BUTTON_HOTKEY_H);

  gtk_label_set_label (atm->hotkey_labels[button_id], text);
}

static int
djb_atm_sqlite3_step (DjbAtm *atm, sqlite3_stmt *stmt, gboolean allow_row)
{
  int result = sqlite3_step (stmt);

  if (result == SQLITE_DONE)
    return result;

  if (allow_row && result == SQLITE_ROW)
    return result;

  fputs (sqlite3_errmsg (atm->sqlite), stderr);
  abort ();
}

/**
 * djb_atm_turn_on:
 *
 * @atm: a DjbAtm
 *
 * turns on the given ATM, i.e. changes its #DjbAtm:state from
 * #DJB_ATM_STATE_OFF to #DJB_ATM_STATE_SHOW_WELCOME. If the ATM was already on,
 * this method has no effect.
 *
 * Returns: whether the ATM was off and was turned on by this call
 */
gboolean
djb_atm_turn_on (DjbAtm *atm)
{
  g_return_val_if_fail (DJB_IS_ATM (atm), FALSE);

  if (atm->state == DJB_ATM_STATE_OFF)
    {
      djb_atm_set_state (atm, DJB_ATM_STATE_SHOW_WELCOME);
      return TRUE;
    }

  return FALSE;
}

/**
 * djb_atm_push_button:
 *
 * @atm: a DjbAtm
 * @button_id: the #DjbAtmButtonId of the button to push
 *
 * pushes @button_id on @atm
 *
 * Returns: whether the pressed button was accepted, i.e. whether it is a valid
 * input for the current #DjbAtm:state, did not result in any validation errors,
 * etc.
 */
gboolean
djb_atm_push_button (DjbAtm *atm, DjbAtmButtonId button_id)
{
  DjbAtmFormHandler handler = djb_atm_form_functions[atm->state].handler;

  if (handler == NULL || !handler (atm, button_id))
    {
      djb_atm_beep (atm);
      return FALSE;
    }

  return TRUE;
}
