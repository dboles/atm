#ifndef DJB_ATM_HOTKEY_SET_H
#define DJB_ATM_HOTKEY_SET_H

#include <glib-object.h>
#include <gtk/gtk.h>

G_BEGIN_DECLS

#define DJB_TYPE_ATM_HOTKEY_SET djb_atm_hotkey_set_get_type ()
G_DECLARE_FINAL_TYPE (DjbAtmHotkeySet, djb_atm_hotkey_set, DJB, ATM_HOTKEY_SET, GObject)

G_END_DECLS

#endif // DJB_ATM_HOTKEY_SET_H
