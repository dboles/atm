#include "config.h"

#include "DjbAtm.h"
#include "lib/DjbAtm-resource.h"

static gboolean
timeout_turn_on (void *user_data)
{
  DjbAtm *atm = DJB_ATM (user_data);
  djb_atm_turn_on (atm);

  return G_SOURCE_REMOVE;
}

static void
on_window_hide (void *user_data)
{
  DjbAtm *atm = DJB_ATM (user_data);
  g_object_unref (atm);
}

static void
on_application_activate (GApplication *application, void *user_data)
{
  (void)user_data;

  GtkSettings *settings = gtk_settings_get_default ();
  g_assert (settings);
  g_object_set (settings,
                "gtk-theme-name", "Adwaita",
                "gtk-application-prefer-dark-theme", FALSE,
                NULL);

  g_assert (djb_atm_get_resource ());
  GdkScreen *screen = gdk_screen_get_default ();
  g_assert (screen);
  GtkCssProvider *css_provider = gtk_css_provider_new ();
  gtk_css_provider_load_from_resource (css_provider, "/org/djb/atm/style.css");
  gtk_style_context_add_provider_for_screen (screen,
                                             GTK_STYLE_PROVIDER (css_provider),
                                             GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);

  DjbAtm *atm = djb_atm_new (DJB_ATM_CONFIG_DATABASE);
  GtkWindow *window = djb_atm_get_window (atm);
  g_signal_connect_swapped (window, "hide",
                            G_CALLBACK (on_window_hide), atm);
  gtk_application_add_window (GTK_APPLICATION (application), window);

  // Delay this to avoid the animation getting lost during the chaos of startup
  g_timeout_add_seconds (1, timeout_turn_on, atm);
}

int
main (int argc, char **argv)
{
  GtkApplication *application = gtk_application_new ("org.djb.atm",
                                                     G_APPLICATION_FLAGS_NONE);
  g_signal_connect (application, "activate",
                    G_CALLBACK (on_application_activate), NULL);

  int result = g_application_run (G_APPLICATION (application), argc, argv);
  g_object_unref (application);
  return result;
}
