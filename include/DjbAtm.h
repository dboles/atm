#ifndef DJB_ATM_ATM_H
#define DJB_ATM_ATM_H

#include <glib-object.h>
#include <gtk/gtk.h>

G_BEGIN_DECLS

#define DJB_TYPE_ATM djb_atm_get_type ()
G_DECLARE_FINAL_TYPE (DjbAtm, djb_atm, DJB, ATM, GObject)

/**
 * DjbAtmState:
 *
 * @DJB_ATM_STATE_OFF: The ATM is off
 *
 * @DJB_ATM_STATE_SHOW_WELCOME: The ATM is showing the Welcome form
 *
 * @DJB_ATM_STATE_PROMPT_ACCOUNT: The ATM is asking for an account number
 *
 * @DJB_ATM_STATE_SHOW_ACCOUNT_NONEXISTENT: The ATM is warning that the entered
 *   account does not exist
 *
 * @DJB_ATM_STATE_SHOW_LOCKED: The ATM is warning that the entered account is
 *   locked or disabled in the database
 *
 * @DJB_ATM_STATE_PROMPT_PIN: The ATM is asking for the PIN for the entered
 *   account
 *
 * @DJB_ATM_STATE_SHOW_PIN_FAILED: The ATM is warning that the PIN was wrong
 *
 * @DJB_ATM_STATE_PROMPT_OPTION: The ATM is asking what the user wants to do
 *
 * @DJB_ATM_STATE_PROMPT_REMOVE_CARD: The ATM is telling the user to remove
 *   their card
 *
 * @DJB_ATM_STATE_SHOW_BALANCE: The ATM is displaying the user's current and
 *   available balances, as well as the minimal denomination withdrawable
 *
 * @DJB_ATM_STATE_SHOW_BALANCE_AS_ERROR: The ATM is displaying the balances as
 *   above, plus an error that the user tried to withdraw an invalid amount.
 *
 * @DJB_ATM_STATE_PROMPT_WITHDRAW: The ATM is asking how much to withdraw
 *
 * @DJB_ATM_STATE_PROMPT_WITHDRAW_RECEIPT: The ATM is asking how much to
 *   withdraw and will print a receipt as part of the withdrawal process
 *
 * @DJB_ATM_STATE_SHOW_RECEIPT: The ATM is showing the requested receipt on
 *   screen
 *
 * @DJB_ATM_STATE_PROMPT_REMOVE_CASH: The ATM is telling the user to remove the
 *   dispensed cash, their card, and optionally the receipt
 *
 * @DJB_ATM_STATE_PROMPT_SHUTDOWN: The ATM is confirming whether the manager
 *   wants to shut it down
 *
 * Check the state of a given DjbAtm using its #DjbAtm:state property.
 */
typedef enum
{
  DJB_ATM_STATE_OFF,
  DJB_ATM_STATE_SHOW_WELCOME,
  DJB_ATM_STATE_PROMPT_ACCOUNT,
  DJB_ATM_STATE_SHOW_ACCOUNT_NONEXISTENT,
  DJB_ATM_STATE_SHOW_LOCKED,
  DJB_ATM_STATE_PROMPT_PIN,
  DJB_ATM_STATE_SHOW_PIN_FAILED,
  DJB_ATM_STATE_PROMPT_OPTION,
  DJB_ATM_STATE_PROMPT_REMOVE_CARD,
  DJB_ATM_STATE_SHOW_BALANCE,
  DJB_ATM_STATE_SHOW_BALANCE_AS_ERROR,
  DJB_ATM_STATE_PROMPT_WITHDRAW,
  DJB_ATM_STATE_PROMPT_WITHDRAW_RECEIPT,
  DJB_ATM_STATE_SHOW_RECEIPT,
  DJB_ATM_STATE_PROMPT_REMOVE_CASH,
  DJB_ATM_STATE_PROMPT_SHUTDOWN,

  DJB_ATM_N_STATES /*< skip >*/
} DjbAtmState;

/**
 * DjbAtmButtonId:
 *
 * a button on the front of the ATM, which can be pressed
 */
typedef enum
{
  /* N.B. Keep these first as they're used for indexing! */
  DJB_ATM_BUTTON_HOTKEY_A,
  DJB_ATM_BUTTON_HOTKEY_B,
  DJB_ATM_BUTTON_HOTKEY_C,
  DJB_ATM_BUTTON_HOTKEY_D,
  DJB_ATM_BUTTON_HOTKEY_E,
  DJB_ATM_BUTTON_HOTKEY_F,
  DJB_ATM_BUTTON_HOTKEY_G,
  DJB_ATM_BUTTON_HOTKEY_H,

  /* N.B. Keep these in order as they're used for arithmetic to get a number! */
  DJB_ATM_BUTTON_0,
  DJB_ATM_BUTTON_1,
  DJB_ATM_BUTTON_2,
  DJB_ATM_BUTTON_3,
  DJB_ATM_BUTTON_4,
  DJB_ATM_BUTTON_5,
  DJB_ATM_BUTTON_6,
  DJB_ATM_BUTTON_7,
  DJB_ATM_BUTTON_8,
  DJB_ATM_BUTTON_9,

  /* Do whatever you want with these I guess */
  DJB_ATM_BUTTON_CANCEL,
  DJB_ATM_BUTTON_CLEAR,
  DJB_ATM_BUTTON_ENTER,

  DJB_ATM_N_BUTTONS /*< skip >*/
} DjbAtmButtonId;

DjbAtm      *djb_atm_new          (const char *database);

GtkWindow   *djb_atm_get_window   (DjbAtm *atm);

const char  *djb_atm_get_database (DjbAtm *atm);
DjbAtmState  djb_atm_get_state    (DjbAtm *atm);

gboolean     djb_atm_turn_on      (DjbAtm *atm);
gboolean     djb_atm_push_button  (DjbAtm *atm, DjbAtmButtonId button_id);

G_END_DECLS

#endif // DJB_ATM_ATM_H
