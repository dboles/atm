drop table if exists Status;
drop table if exists Withdrawal;
drop table if exists Account;
drop table if exists System;


create table Status (
	id   integer primary key,
	name text    not null unique
);

insert into Status
values
	(0, 'disabled'),
	(1, 'enabled'),
	(2, 'locked');


create table Account (
	number     char(6) primary key,
	pin        char(4) not null,

	balance    float   not null,
	is_manager integer not null,

	status     integer not null,
	foreign key (status) references Status (id)
);

insert into Account
values
	-- users from assessment PDF, p. 13
	('131400', '1516',  9800.00, 0, 1),
	('111110', '1111', 10000.00, 0, 1),
	('910000', '1112',   210.00, 0, 1),
	('171800', '1920',   100.00, 0, 1),
	('345600', '3456',   100.00, 0, 1),
	('213541', '3525',   300.00, 0, 1),
	('234321', '2323',   100.00, 0, 1),
	('999999', '1234',     0.00, 1, 1), -- the manager
	-- my test account
	('123456', '7890',   240.22, 0, 1),
	-- disabled and locked accounts
	('000000', '0000',     0.00, 0, 0),
	('666666', '6666',     0.00, 0, 2);


create table Withdrawal (
	timestamp datetime not null default current_timestamp,

	account char(6) not null,
	amount  float   not null,

	foreign key (account) references Account (number)
);

insert into Withdrawal (timestamp, account, amount)
values
	('2018-01-01', '123456', 300),
	(date ('now'), '123456', 150);
