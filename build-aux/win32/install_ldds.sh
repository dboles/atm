#!/bin/bash

bindir="$1"
shift

loaders=$(cygpath "$@")

dlls=$(ldd $loaders | cut -d' ' -f3 -s | grep -iv '^/[a-z]/windows' | sort | uniq)

cp -Lpt "$bindir" $dlls
