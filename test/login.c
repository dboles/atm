#include "DjbAtm.h"

#include <glib.h>
#undef G_DISABLE_ASSERT

static void
on_application_activate (GApplication *application, void *user_data)
{
  (void)application;
  (void)user_data;


  // Test that we can create an Atm
  const char *database_set = DJB_ATM_TEST_DATABASE;
  DjbAtm *atm = djb_atm_new (database_set);
  g_assert (DJB_IS_ATM (atm));

  // Test that get_database() returns what was put in
  const char *database_get = djb_atm_get_database (atm);
  g_assert_cmpstr (database_set, ==, database_get);

  // Test that the initial state is SHOW_OFF
  g_assert_cmpint (djb_atm_get_state (atm), ==, DJB_ATM_STATE_OFF);

  // Test that turning it on takes us to SHOW_WELCOME
  g_assert (djb_atm_turn_on (atm));
  g_assert_cmpint (djb_atm_get_state (atm), ==, DJB_ATM_STATE_SHOW_WELCOME);

  // Push the Login button and test that the state is now PROMPT_ACCOUNT
  g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_HOTKEY_H));
  g_assert_cmpint (djb_atm_get_state (atm), ==, DJB_ATM_STATE_PROMPT_ACCOUNT);

  // Push a now-irrelevant button and test that the state did not change
  g_assert (!djb_atm_push_button (atm, DJB_ATM_BUTTON_HOTKEY_H));
  g_assert_cmpint (djb_atm_get_state (atm), ==, DJB_ATM_STATE_PROMPT_ACCOUNT);

  // Push the Cancel button and test that the state is SHOW_WELCOME again
  g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_CANCEL));
  g_assert_cmpint (djb_atm_get_state (atm), ==, DJB_ATM_STATE_SHOW_WELCOME);

  // Push a now-irrelevant button and test that the state did not change
  g_assert (!djb_atm_push_button (atm, DJB_ATM_BUTTON_CANCEL));
  g_assert_cmpint (djb_atm_get_state (atm), ==, DJB_ATM_STATE_SHOW_WELCOME);

  // Move from Welcome to Login again - checks that it works beyond 1st time!
  g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_HOTKEY_H));
  g_assert_cmpint (djb_atm_get_state (atm), ==, DJB_ATM_STATE_PROMPT_ACCOUNT);


  // Log in as the manager

  for (unsigned i = 6; i--;)
    g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_9));

  g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_ENTER));
  g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_1));
  g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_2));
  g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_3));
  g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_4));
  g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_ENTER));
  g_assert_cmpint (djb_atm_get_state (atm), ==, DJB_ATM_STATE_PROMPT_OPTION);


  // Shut down the ATM
  g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_HOTKEY_H));
  g_assert_cmpint (djb_atm_get_state (atm), ==, DJB_ATM_STATE_PROMPT_SHUTDOWN);
  g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_HOTKEY_G));
  g_assert_cmpint (djb_atm_get_state (atm), ==, DJB_ATM_STATE_OFF);


  g_object_unref (atm);
}

int
main (int argc, char **argv)
{
  GtkApplication *application = gtk_application_new ("org.djb.atm.test.login",
                                                     G_APPLICATION_FLAGS_NONE);
  g_signal_connect (application, "activate", G_CALLBACK (on_application_activate), NULL);

  int result = g_application_run (G_APPLICATION (application), argc, argv);
  g_object_unref (application);
  return result;
}
