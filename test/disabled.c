#include "DjbAtm.h"

#include <glib.h>
#undef G_DISABLE_ASSERT

static void
on_application_activate (GApplication *application, void *user_data)
{
  (void)application;
  (void)user_data;

  DjbAtm *atm = djb_atm_new (DJB_ATM_TEST_DATABASE);
  g_assert (DJB_IS_ATM (atm));
  g_assert (djb_atm_turn_on (atm));
  g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_HOTKEY_H));

  for (unsigned i = 6; i--;)
    g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_0));

  g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_ENTER));
  g_assert_cmpint (djb_atm_get_state (atm), ==, DJB_ATM_STATE_SHOW_LOCKED);

  g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_HOTKEY_H));
  g_assert_cmpint (djb_atm_get_state (atm), ==, DJB_ATM_STATE_SHOW_WELCOME);

  g_object_unref (atm);
}

int
main (int argc, char **argv)
{
  GtkApplication *application = gtk_application_new ("org.djb.atm.test.disabled",
                                                     G_APPLICATION_FLAGS_NONE);
  g_signal_connect (application, "activate", G_CALLBACK (on_application_activate), NULL);

  int result = g_application_run (G_APPLICATION (application), argc, argv);
  g_object_unref (application);
  return result;
}
