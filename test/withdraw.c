#include "DjbAtm.h"

#include <glib.h>
#undef G_DISABLE_ASSERT

static void
login_as_123456_7890 (DjbAtm *atm)
{
  g_assert_cmpint (djb_atm_get_state (atm), ==, DJB_ATM_STATE_SHOW_WELCOME);

  g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_HOTKEY_H));
  g_assert_cmpint (djb_atm_get_state (atm), ==, DJB_ATM_STATE_PROMPT_ACCOUNT);

  for (unsigned i = 1; i <= 6; ++i)
    g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_0 + i));

  g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_ENTER));

  for (unsigned i = 7; i <= 10; ++i)
    g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_0 + i % 10));

  g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_ENTER));
  g_assert_cmpint (djb_atm_get_state (atm), ==, DJB_ATM_STATE_PROMPT_OPTION);
}

static void
on_application_activate (GApplication *application, void *user_data)
{
  (void)application;
  (void)user_data;

  DjbAtm *atm = djb_atm_new (DJB_ATM_TEST_DATABASE);
  g_assert (DJB_IS_ATM (atm));
  g_assert (djb_atm_turn_on (atm));
  login_as_123456_7890 (atm);

  // Test that can show balance and go back to options
  g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_HOTKEY_A)); // Balance
  g_assert_cmpint (djb_atm_get_state (atm), ==, DJB_ATM_STATE_SHOW_BALANCE); // initial balance = £150
  g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_HOTKEY_A)); // Back
  g_assert_cmpint (djb_atm_get_state (atm), ==, DJB_ATM_STATE_PROMPT_OPTION);

  // Test that user cannot enter 0 alone, only after entering non-zero digits
  g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_HOTKEY_C)); // Withdraw
  g_assert_cmpint (djb_atm_get_state (atm), ==, DJB_ATM_STATE_PROMPT_WITHDRAW);
  g_assert (!djb_atm_push_button (atm, DJB_ATM_BUTTON_0));
  g_assert_cmpint (djb_atm_get_state (atm), ==, DJB_ATM_STATE_PROMPT_WITHDRAW);

  // Test that user cannot withdraw more than they have available
  g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_2));
  g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_5));
  g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_0));
  g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_ENTER));
  g_assert_cmpint (djb_atm_get_state (atm), ==, DJB_ATM_STATE_SHOW_BALANCE_AS_ERROR);

  // Successfully withdraw using number keys
  g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_HOTKEY_A)); // Back
  g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_HOTKEY_C)); // Withdraw
  g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_3));
  g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_0));
  g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_ENTER));
  g_assert_cmpint (djb_atm_get_state (atm), ==, DJB_ATM_STATE_PROMPT_REMOVE_CASH); // balance = £120, avail = £70
  g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_HOTKEY_H)); // Log out

  // Successfully withdraw using a hotkey
  login_as_123456_7890 (atm);
  g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_HOTKEY_C)); // Withdraw
  g_assert_cmpint (djb_atm_get_state (atm), ==, DJB_ATM_STATE_PROMPT_WITHDRAW);
  g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_HOTKEY_C)); // £30
  g_assert_cmpint (djb_atm_get_state (atm), ==, DJB_ATM_STATE_PROMPT_REMOVE_CASH); // balance = £90, avail = £40
  g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_HOTKEY_H)); // Log out

  // Test that user cannot withdraw more than they have available *now*
  login_as_123456_7890 (atm);
  g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_HOTKEY_C)); // Withdraw
  g_assert_cmpint (djb_atm_get_state (atm), ==, DJB_ATM_STATE_PROMPT_WITHDRAW);
  g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_HOTKEY_D)); // £50
  g_assert_cmpint (djb_atm_get_state (atm), ==, DJB_ATM_STATE_SHOW_BALANCE_AS_ERROR);
  g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_CANCEL)); // Log out
  g_assert_cmpint (djb_atm_get_state (atm), ==, DJB_ATM_STATE_PROMPT_REMOVE_CARD);
  g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_HOTKEY_H)); // Remove card
  g_assert_cmpint (djb_atm_get_state (atm), ==, DJB_ATM_STATE_SHOW_WELCOME);

  // Test that user cannot withdraw more than is available *now*, after reboot
  g_object_unref (atm);
  atm = djb_atm_new (DJB_ATM_TEST_DATABASE);
  g_assert (DJB_IS_ATM (atm));
  g_assert (djb_atm_turn_on (atm));
  login_as_123456_7890 (atm);
  g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_HOTKEY_C)); // withdraw
  g_assert_cmpint (djb_atm_get_state (atm), ==, DJB_ATM_STATE_PROMPT_WITHDRAW);
  g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_HOTKEY_D)); // £50
  g_assert_cmpint (djb_atm_get_state (atm), ==, DJB_ATM_STATE_SHOW_BALANCE_AS_ERROR);

  // Test that user can withdraw a final amount and get a receipt
  g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_HOTKEY_A)); // Back
  g_assert_cmpint (djb_atm_get_state (atm), ==, DJB_ATM_STATE_PROMPT_OPTION);
  g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_HOTKEY_D)); // Withdraw + receipt
  g_assert_cmpint (djb_atm_get_state (atm), ==, DJB_ATM_STATE_PROMPT_WITHDRAW_RECEIPT);
  g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_HOTKEY_C)); // £40 of avail. £40
  g_assert_cmpint (djb_atm_get_state (atm), ==, DJB_ATM_STATE_SHOW_RECEIPT);
  g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_HOTKEY_H)); // Next
  g_assert_cmpint (djb_atm_get_state (atm), ==, DJB_ATM_STATE_PROMPT_REMOVE_CASH);
  g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_HOTKEY_H)); // Remove cash & card
  g_assert_cmpint (djb_atm_get_state (atm), ==, DJB_ATM_STATE_SHOW_WELCOME);

  g_object_unref (atm);
}

int
main (int argc, char **argv)
{
  GtkApplication *application = gtk_application_new ("org.djb.atm.test.withdraw",
                                                     G_APPLICATION_FLAGS_NONE);
  g_signal_connect (application, "activate", G_CALLBACK (on_application_activate), NULL);

  int result = g_application_run (G_APPLICATION (application), argc, argv);
  g_object_unref (application);
  return result;
}
