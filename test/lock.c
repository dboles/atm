#include "DjbAtm.h"

#include <glib.h>
#undef G_DISABLE_ASSERT

static void
on_application_activate (GApplication *application, void *user_data)
{
  (void)application;
  (void)user_data;


  // Test that we can create an Atm
  const char *database_set = DJB_ATM_TEST_DATABASE;
  DjbAtm *atm = djb_atm_new (database_set);
  g_assert (DJB_IS_ATM (atm));

  // Test that get_database() returns what was put in
  const char *database_get = djb_atm_get_database (atm);
  g_assert_cmpstr (database_set, ==, database_get);

  // Test that the initial state is SHOW_OFF
  g_assert_cmpint (djb_atm_get_state (atm), ==, DJB_ATM_STATE_OFF);

  // Test that turning it on takes us to SHOW_WELCOME
  g_assert (djb_atm_turn_on (atm));
  g_assert_cmpint (djb_atm_get_state (atm), ==, DJB_ATM_STATE_SHOW_WELCOME);

  // Push the Login button and test that the state is now PROMPT_ACCOUNT
  g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_HOTKEY_H));
  g_assert_cmpint (djb_atm_get_state (atm), ==, DJB_ATM_STATE_PROMPT_ACCOUNT);


  // Enter a valid account number

  for (unsigned i = 1; i <= 6; ++i)
    g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_0 + i));

  g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_ENTER));


  // Fail the PIN once

  g_assert_cmpint (djb_atm_get_state (atm), ==, DJB_ATM_STATE_PROMPT_PIN);

  for (unsigned i = 4; i--;)
    g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_0));

  g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_ENTER));
  g_assert_cmpint (djb_atm_get_state (atm), ==, DJB_ATM_STATE_SHOW_PIN_FAILED);


  // Fail the PIN a 2nd time

  g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_HOTKEY_H));
  g_assert_cmpint (djb_atm_get_state (atm), ==, DJB_ATM_STATE_PROMPT_PIN);

  for (unsigned i = 4; i--;)
    g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_0));

  g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_ENTER));
  g_assert_cmpint (djb_atm_get_state (atm), ==, DJB_ATM_STATE_SHOW_PIN_FAILED);

  // Fail the PIN a 3rd time

  g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_HOTKEY_H));
  g_assert_cmpint (djb_atm_get_state (atm), ==, DJB_ATM_STATE_PROMPT_PIN);

  for (unsigned i = 4; i--;)
    g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_0));

  g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_ENTER));
  g_assert_cmpint (djb_atm_get_state (atm), ==, DJB_ATM_STATE_SHOW_LOCKED);

  g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_HOTKEY_H));
  g_assert_cmpint (djb_atm_get_state (atm), ==, DJB_ATM_STATE_SHOW_WELCOME);


  // Test that locked status saved to database and persists between power-cycles

  g_object_unref (atm);
  atm = djb_atm_new (database_set);
  g_assert (DJB_IS_ATM (atm));
  g_assert (djb_atm_turn_on (atm));
  g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_HOTKEY_H));

  for (unsigned i = 1; i <= 6; ++i)
    g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_0 + i));

  g_assert (djb_atm_push_button (atm, DJB_ATM_BUTTON_ENTER));
  g_assert_cmpint (djb_atm_get_state (atm), ==, DJB_ATM_STATE_SHOW_LOCKED);


  g_object_unref (atm);
}

int
main (int argc, char **argv)
{
  GtkApplication *application = gtk_application_new ("org.djb.atm.test.lock",
                                                     G_APPLICATION_FLAGS_NONE);
  g_signal_connect (application, "activate", G_CALLBACK (on_application_activate), NULL);

  int result = g_application_run (G_APPLICATION (application), argc, argv);
  g_object_unref (application);
  return result;
}
